﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChanger : MonoBehaviour
{

    private Color begin = Color.cyan;
    private Color end = Color.red;
    private Color[] colors = { Color.red, Color.magenta, Color.green, Color.cyan, Color.blue, Color.yellow };
    private int ind, ind2;
    float duration = 1.0f;
    float lerp;
    
    Renderer rend;
    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
        ind = 0;
        //ind2 = 0;
        InvokeRepeating("randColor", 0f, duration);
    }

    // Update is called once per frame
    void Update()
    {
        lerp = Mathf.MoveTowards(lerp, 1f, Time.deltaTime);
        rend.material.color = Color.Lerp(begin, end, lerp);
    }

    void randColor()
    {
        lerp = 0f;
        ind = Random.Range(0, colors.Length);
        //Begin is set to the current color so the transformation blends well. 
        begin = rend.material.color;
        end = colors[ind];
    }
}
