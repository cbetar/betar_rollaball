﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour
{

    private int x, y, z;

    // Start is called before the first frame update
    void Start()
    {
        x = 0;
        y = 10;
        z = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //Spins the circle of pick ups around in a circle clockwise.
        transform.Rotate(new Vector3(x, y, z) * Time.deltaTime);
    }

    private void FixedUpdate()
    {
       
    }
}
